export let style = new PIXI.TextStyle({
  wordWrap: true,
  wordWrapWidth: 20,
  fontFamily: "Courier",
  fontWeight: "Bold",
  fontSize: 26,
  fill: "white",
  stroke: '#0000FF',
  strokeThickness: 4,
});