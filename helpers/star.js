export class Star extends PIXI.Sprite {
  constructor(parallax) {
    super(PIXI.loader.resources["images/star.png"].texture)

    this.scale.set(parallax * 0.03, parallax * 0.03);

    this.x = Math.random() * window.innerWidth;
    this.y = Math.random() * window.innerHeight;

    this.vx = parallax * 0.5;

    this.tint = Star.colors[Math.floor(Math.random() * Star.colors.length)];

  }

  static get colors() {
    return [
      0xFF0000,
      0x800000,
      0xFFFF00,
      0x808000,
      0x00FF00,
      0x008000,
      0x00FFFF,
      0x008080,
      0x0000FF,
      0x000080,
      0xFF00FF,
      0x800080,
    ]
  }

}