export let style2 = new PIXI.TextStyle({
  align: "center",
  fontFamily: "Courier",
  fontWeight: "Bold",
  fontSize: 26,
  fill: "white",
  stroke: '#0000FF',
  strokeThickness: 4,
});