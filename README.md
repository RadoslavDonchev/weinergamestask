# Weiner Games TASK

## Description

A small side-scroller game made with Pixi.JS - a 2D webGL renderer. The player controls a spaceship and has to dogde enemies and keep his fuel from running out.

### Stack

Front-end:
- SPA - Pixi.JS
Git repository: https://gitlab.com/RadoslavDonchev/weinergamestask

----------

# Functionality overview

- Basic movement up and down y axis
- Dynamic background made up of individual sprites
- Different enemy types firing projectiles
- Fuel mechanic
- End game screen upon fail state

# Author

Radoslav Donchev