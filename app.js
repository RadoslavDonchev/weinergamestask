import {
  keyboard
} from "./helpers/keyboard.js";
import {
  Star
} from "./helpers/star.js";
import {
  style
} from "./helpers/text-style.js";
import {
  style2
} from "./helpers/text-style2.js";

let mainShip;
let state;
let stars;
let distanceMessage;
let fuelMessage;
let enemy1;
let enemy2;
let enemy3;
let bullets;
let enemies;
let fuel;
let shipFuel = 100;
let distance = 0;
let distanceText = "Lightyears: ";
let fuelText = "Fuel: ";
let b;
let gameScene;
let gameOverScene;

let app = new PIXI.Application({
  width: window.innerWidth,
  height: window.innerHeight
});

document.body.appendChild(app.view);

app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoDensity = true;

PIXI.loader
  .add("images/main-ship.png")
  .add("images/star.png")
  .add("images/enemy1.png")
  .add("images/enemy2.png")
  .add("images/enemy3.png")
  .add("images/bullet.png")
  .add("images/fuel.png")
  .load(setup);

function setup() {
  gameScene = new PIXI.Container();
  app.stage.addChild(gameScene);

  createStars();

  bullets = new PIXI.Container();
  gameScene.addChild(bullets);

  enemies = new PIXI.Container();
  gameScene.addChild(enemies);

  createMainShip();

  createEnemy1();

  setTimeout(function () {
    createEnemy2();
  }, 4000);

  setTimeout(function () {
    createEnemy3();
  }, 10000);

  setInterval(function () {
    createFuel();
  }, 13000);

  setInterval(function () {
    shipFuel--;
  }, 300)

  assignKeys();

  createUI();

  b = new Bump(PIXI);

  state = play;

  app.ticker.add(gameLoop);

}

function gameLoop() {
  state();
}



function play() {

  //Limit player from going off-screen
  if (b.hit({
      x: 100,
      y: 0
    }, mainShip, true)) {
    mainShip.vy = 0;
    mainShip.y += 1;
  }

  if (b.hit({
      x: 100,
      y: window.innerHeight
    }, mainShip)) {
    mainShip.vy = 0;
    mainShip.y -= 1;
  }

  //Move mainShip if it has velocity
  mainShip.y += mainShip.vy;

  //End game if fuel is depleated
  if (shipFuel < 0) {
    state = end;
  }

  //Fuel pick up logic
  if (fuel) {
    fuel.x -= 2;

    if (fuel.x < -fuel.width) {
      gameScene.removeChild(fuel);
    }

    if (b.hit(mainShip, fuel)) {
      gameScene.removeChild(fuel);
      fuel = undefined;
      shipFuel += 100;
    }

  }

  //Move and loop enemies if they have spawned
  enemy1.x -= enemy1.vx;
  loopSprite(enemy1);

  if (enemy2) {
    enemy2.x -= enemy2.vx;
    loopSprite(enemy2);
    if (b.hitTestRectangle(enemy2, mainShip)) {
      state = end;
    }
  }

  if (enemy3) {
    enemy3.x -= enemy3.vx;
    loopSprite(enemy3);
    if (b.hitTestRectangle(enemy3, mainShip)) {
      state = end;
    }
  }

  //Loop through bullets, move and remove them
  bullets.children.forEach((child) => {
    child.x -= child.vx;
    child.y -= child.vy;
    if (child.x < -child.width) {
      bullets.removeChild(child);
    }
  });

  //Loop through stars and move them
  stars.children.forEach((child) => {
    child.x -= child.vx;
    loopSprite(child);
  });

  //Bullet hit detection only works some of the time!
  if (b.hit(mainShip, bullets.children)) {
    state = end;
  }

  //Update distance
  distance++;
  distanceMessage.text = distanceText + distance;

  //Update fuel
  fuelMessage.text = fuelText + shipFuel;

  //Hit detection with single sprite works all the time
  if (b.hitTestRectangle(enemy1, mainShip)) {
    state = end;
  }

}






// FUNCTIONS

function createUI() {
  distanceMessage = new PIXI.Text(distanceText);
  distanceMessage.style = style;
  gameScene.addChild(distanceMessage);

  fuelMessage = new PIXI.Text(fuelText);
  fuelMessage.style = style;
  fuelMessage.position.set(window.innerWidth - fuelMessage.width, 0);
  gameScene.addChild(fuelMessage);
}

function createFuel() {
  fuel = new PIXI.Sprite(
    PIXI.loader.resources["images/fuel.png"].texture
  );
  fuel.x = window.innerWidth;
  fuel.y = Math.random() * window.innerHeight;
  fuel.scale.set(0.3, 0.3);
  fuel.anchor.set(0.5, 0.5);
  fuel.vy = 0;
  gameScene.addChild(fuel);
}

function createEnemy1() {
  enemy1 = new PIXI.Sprite(
    PIXI.loader.resources["images/enemy1.png"].texture
  );
  enemy1.x = window.innerWidth + enemy1.width;
  enemy1.y = Math.random() * window.innerHeight;
  enemy1.scale.set(0.3, 0.4);
  enemy1.anchor.set(0.5, 0.5);
  enemy1.vx = 4;
  enemies.addChild(enemy1);
}

function createEnemy2() {
  enemy2 = new PIXI.Sprite(
    PIXI.loader.resources["images/enemy2.png"].texture
  );
  enemy2.x = window.innerWidth + enemy2.width;
  enemy2.y = Math.random() * window.innerHeight;
  enemy2.scale.set(0.3, 0.4);
  enemy2.anchor.set(0.5, 0.5);
  enemy2.vx = 3;
  enemies.addChild(enemy2);

  setInterval(function () {
    let bullet = new PIXI.Sprite(
      PIXI.loader.resources["images/bullet.png"].texture
    );
    bullet.x = enemy2.x - enemy2.width;
    bullet.y = enemy2.y;
    bullet.scale.set(0.05, 0.05);
    bullet.anchor.set(0.5, 0.5);
    bullet.vx = 5;
    bullet.vy = 0;
    bullets.addChild(bullet);
  }, 2000)
}

function createEnemy3() {
  enemy3 = new PIXI.Sprite(
    PIXI.loader.resources["images/enemy3.png"].texture
  );
  enemy3.x = window.innerWidth + enemy3.width;
  enemy3.y = Math.random() * window.innerHeight;
  enemy3.scale.set(0.3, 0.4);
  enemy3.anchor.set(0.5, 0.5);
  enemy3.vx = 2;
  enemies.addChild(enemy3);

  setInterval(function () {
    let bullet1 = new PIXI.Sprite(
      PIXI.loader.resources["images/bullet.png"].texture
    );
    bullet1.x = enemy3.x - enemy3.width;
    bullet1.y = enemy3.y - 20;
    bullet1.scale.set(0.05, 0.05);
    bullet1.anchor.set(0.5, 0.5);
    bullet1.vx = 7;
    bullet1.vy = 7;
    bullets.addChild(bullet1);
    let bullet2 = new PIXI.Sprite(
      PIXI.loader.resources["images/bullet.png"].texture
    );
    bullet2.x = enemy3.x - enemy3.width;
    bullet2.y = enemy3.y + 20;
    bullet2.scale.set(0.05, 0.05);
    bullet2.anchor.set(0.5, 0.5);
    bullet2.vx = 7;
    bullet2.vy = -7;
    bullets.addChild(bullet2);
  }, 3000)
}

function assignKeys() {
  let up = keyboard("ArrowUp");
  let down = keyboard("ArrowDown");
  let enter = keyboard("Enter");

  enter.press = () => {
    location.reload();
  };

  up.press = () => {
    mainShip.vy = -6;
  };
  up.release = () => {
    if (!down.isDown) {
      mainShip.vy = 0;
    }
  };

  down.press = () => {
    mainShip.vy = 6;
  };
  down.release = () => {
    if (!up.isDown) {
      mainShip.vy = 0;
    }
  };
}

function createGameOverScene() {
  gameOverScene = new PIXI.Container();
  app.stage.addChild(gameOverScene);
  let gameOverMessage = new PIXI.Text('Game over!\n Your score is:\n' + distance + '\nPress Enter to restart.');
  gameOverMessage.style = style2;
  gameOverMessage.position.set(window.innerWidth / 2, window.innerHeight / 2);
  gameOverMessage.anchor.set(0.5, 0.5);
  gameOverScene.addChild(gameOverMessage);
  gameOverScene.visible = false;
}

function createStars() {
  stars = new PIXI.Container();
  for (let i = 0; i < 30; i++) {
    if (i < 10) stars.addChild(new Star(1));
    else if (i < 20) stars.addChild(new Star(2));
    else stars.addChild(new Star(3));
  }
  gameScene.addChild(stars);
}

function createMainShip() {
  mainShip = new PIXI.Sprite(
    PIXI.loader.resources["images/main-ship.png"].texture
  );
  mainShip.x = 100;
  mainShip.y = app.renderer.height / 2;
  mainShip.scale.set(0.3, 0.5);
  mainShip.anchor.set(0.5, 0.5);
  mainShip.vy = 0;
  gameScene.addChild(mainShip);
}

function loopSprite(sprite) {
  if (sprite.x < -sprite.width) {
    sprite.x *= -1;
    sprite.x += window.innerWidth;
    sprite.y = Math.random() * window.innerHeight;
  }
}


function end() {
  gameScene.visible = false;
  createGameOverScene();
  gameOverScene.visible = true;
}